//
//  SideMenuTableViewCell.swift
//  SideMenu-Boiler
//
//  Created by Jansen Ducusin on 3/3/21.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var menuLabel: UILabel!
    
    static let identifier = "SideMenuTableViewCell"
    
    static func nib() -> UINib{
        return UINib(nibName: "SideMenuTableViewCell", bundle: nil)
    }
    
    func configure(with menuItem: MenuItem){
        self.menuLabel.text = menuItem.rawValue
        
        switch menuItem {
        case .home:
            self.menuIcon.image = UIImage(systemName: "house.fill")
        case .info:
            self.menuIcon.image = UIImage(systemName: "info.circle.fill")
        case .settings:
            self.menuIcon.image = UIImage(systemName: "gearshape.fill")
        }
    }
    
}
