//
//  ContainerViewController.swift
//  SideMenu-Boiler
//
//  Created by Jansen Ducusin on 3/3/21.
//
import SideMenu
import Foundation
import UIKit

class ContainerViewController: UIViewController {
    private var sideMenu: SideMenuNavigationController?
    
    @IBOutlet weak var infoContainerView: UIView!
    @IBOutlet weak var settingsContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSideMenu()
    }
    
    @IBAction func menuButtonActionTap(){
        if let sideMenu = sideMenu {
            present(sideMenu, animated: true)
        }
    }
    
    private func setupSideMenu(){
        let sideMenuTVC = SideMenuTableViewController(with: MenuItem.allCases)
        sideMenuTVC.delegate = self
        self.sideMenu = SideMenuNavigationController(rootViewController: sideMenuTVC)
        
        if let sideMenu = sideMenu {
            sideMenu.leftSide = true
            SideMenuManager.default.leftMenuNavigationController = sideMenu
            SideMenuManager.default.addPanGestureToPresent(toView: view)
        }
    }
}

extension ContainerViewController: SideMenuTableViewControllerDelegate{
    func SideMenuTableViewControllerDidSelectItem(menuItem: MenuItem) {
        switch menuItem {
        case .home:
            self.infoContainerView.isHidden = true
            self.settingsContainerView.isHidden = true
        case .info:
            self.infoContainerView.isHidden = false
            self.settingsContainerView.isHidden = true
        case .settings:
            self.infoContainerView.isHidden = true
            self.settingsContainerView.isHidden = false
        }
        
        self.title = menuItem.rawValue
    }
}
