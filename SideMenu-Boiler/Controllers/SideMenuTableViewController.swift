//
//  SideMenuTableViewController.swift
//  SideMenu-Boiler
//
//  Created by Jansen Ducusin on 3/3/21.
//


import Foundation
import UIKit

enum MenuItem: String, CaseIterable {
    case home = "Home"
    case info = "Info"
    case settings = "Settings"
    
    static var allCases: [MenuItem]{
        return [.home, .info, .settings]
    }
}

protocol SideMenuTableViewControllerDelegate {
    func SideMenuTableViewControllerDidSelectItem(menuItem: MenuItem)
}

class SideMenuTableViewController: UITableViewController{
    
    var delegate: SideMenuTableViewControllerDelegate?
    private let menuItems: [MenuItem]
    private let backgroundColor = UIColor.darkGray
    private let textColor = UIColor.white
    
    init(with menuItems:[MenuItem]){
        self.menuItems = menuItems
        super.init(nibName: nil, bundle: nil)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = backgroundColor
        self.tableView.register(SideMenuTableViewCell.nib(), forCellReuseIdentifier: "SideMenuTableViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuTableViewCell.identifier, for: indexPath) as! SideMenuTableViewCell
        
        cell.configure(with: menuItems[indexPath.row])
        cell.menuLabel?.textColor = textColor
        cell.backgroundColor = backgroundColor
        cell.contentView.backgroundColor = backgroundColor
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedMenuItem = menuItems[indexPath.row]
        self.dismiss(animated: true, completion: nil)
        self.delegate?.SideMenuTableViewControllerDidSelectItem(menuItem: selectedMenuItem)
    }

}
