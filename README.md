**Side Menu Boilerplate**

A boilerplate for apps that uses a side sliding menu.

---

## Dependency

- SideMenu - https://github.com/jonkykong/SideMenu

---

## Demo

![](/Previews/preview.gif)
